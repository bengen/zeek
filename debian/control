Source: bro
Section: net
Priority: optional
Maintainer: Hilko Bengen <bengen@debian.org>
Build-Depends: debhelper (>= 12~),
 cmake,
 binpac (>= 0.54-2~),
 btest,
 bifcl,
 libbroker-dev, libcaf-dev (>= 0.16),
 bison,
 flex,
 rsync,
 libcurl4-openssl-dev | libcurl-dev,
 libreadline-dev,
 libmaxminddb-dev,
 libgoogle-perftools-dev,
 libpcap0.8-dev | libpcap-dev,
 libsqlite3-dev,
 libssl-dev (>= 1.1),
 libxml2-dev,
 zlib1g-dev,
 libkrb5-dev,
 libmmdb2-dev,
Standards-Version: 4.3.0
Homepage: http://www.zeek.org/
Vcs-Git: https://salsa.debian.org/bro-team/bro.git
Vcs-Browser: https://salsa.debian.org/bro-team/bro

Package: bro
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 bro-common
Built-Using: ${misc:Built-Using},
Description: passive network traffic analyzer
 Bro is primarily a security monitor that inspects all traffic on a link in
 depth for signs of suspicious activity. More generally, however, Bro
 supports a wide range of traffic analysis tasks even outside of the
 security domain, including performance measurements and helping with
 trouble-shooting.
 .
 Bro comes with built-in functionality for a range of analysis and
 detection tasks, including detecting malware by interfacing to external
 registries, reporting vulnerable versions of software seen on the network,
 identifying popular web applications, detecting SSH brute-forcing,
 validating SSL certificate chains, among others.

Package: bro-common
Architecture: all
Depends: ${misc:Depends}
Description: passive network traffic analyzer -- architecture-independent parts
 This package contains the architecture-independent parts for the Bro
 network security monitor.

Package: bro-dev
Architecture: all
Depends: ${misc:Depends}
Description: passive network traffic analyzer -- development files
 This package contains the header files needed for building extensions
 for the Bro network security monitor.
